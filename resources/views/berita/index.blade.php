@extends('layout.master')

@section('judul')
    Data Berita
@endsection

@section('content')
<div class="table-responsive">
  <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
      <thead>
          <tr>
              <th width="25px">No</th>
              <th width="100px">Judul</th>
              <th width="275px">isi</th>
              <th width="225px">Thumbnail</th>
              <th width="50px">Kategori</th>
              <th width="200px">Aksi</th>
          </tr>
      </thead>
      <tbody>
        <?php $no = 0; ?>
        @forelse ($berita as $item)
            <tr>
                <td>{{$no = $no + 1}}</td>
                <td>{{$item->judul}}</td>
                <td>{{Str::limit($item->isi, 50, $end='.......')}}</td>
                <td><img src="{{asset('gambar/' . $item->thumbnail)}}" style="object-fit: contain; height:200px" width="200px"></td>
                <td>
                    {{$item->kategori_id}}               
                </td>
                <td>
                    <form action="/berita/{{$item->id}}" method="POST">
                    @csrf
                    @method('DELETE')
                    <a href="/berita/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                    <a href="/berita/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                    <input type="submit" value="Delete" class="btn btn-danger btn-sm" onClick="return confirm('Anda yakin ingin menghapus data : ?')">
                    </form>
                </td>
            </tr>      
        @empty
            <tr>
               <td><h4>Data berita belum ada</h4></td>
            </tr> 
        @endforelse
          
      </tbody>
  </table>
@endsection