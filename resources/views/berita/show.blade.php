@extends('layout.master')

@section('judul')
    Data Detail Berita
@endsection

@section('content')
  <img src="{{asset('gambar/'.$berita->thumbnail)}}" style="object-fit:contain; width: 50%; height:50%; display:block; margin-left: auto; margin-right: auto; margin-bottom: 20px" alt="Generic placeholder image">
  <h4>{{"Judul : " . $berita->judul}}</h4>
  <h4>{{"Kategori : " . $berita->kategori_id}}</h4>
  <p>{{$berita->isi}}</p>
  <a href="/berita" class="btn btn-secondary">Kembali</a>
@endsection