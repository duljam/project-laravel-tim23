<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// CRUD Berita
// Route::get('/master', 'BeritaController@index');
Route::resource('berita', 'BeritaController');

// CRUD Kategori
Route::resource('kategori', 'KategoriController');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
